<?php namespace Comayers\DOM;

use Closure;
use Exception;

class DOM {

	/**
	 * Set the element
	 *
	 * @param  string    $tag
	 * @param  \Closure  $callback
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function __call($tag, $callback) {
		$html = new DOMBuilder();
		$html->tag($tag);

		if(!isset($callback[0])) return $html;
		elseif(!$callback[0] instanceof Closure && !$this->canBeString($callback[0])) throw new Exception('Argument 1 must be a closure, string or null. (' . gettype($callback[0]) . ' given)');

		if($callback[0] instanceof Closure) { 
			call_user_func($callback[0], $html);
		} elseif($this->canBeString($callback[0])) {
			$html->html((string) $callback[0]);
		}

		return $html;
	}

	private function canBeString($value) {
		return ((is_object($value) && method_exists($value, '__toString')) || is_null($value) || is_scalar($value));
	}

}