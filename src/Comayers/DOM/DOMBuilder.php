<?php namespace Comayers\DOM;

use \Closure;
use \DOMDocument;
use Exception;

class DOMBuilder {
	
	/**
	 * Tag attributes
	 *
	 * @var array
	 */
	protected $attributes = array();

	/**
	 * Closures and text/html of nodes underneath this tag
	 *
	 * @var array
	 */
	protected $children = array();

	/**
	 * HTML Tag
	 *
	 * @var string
	 */
	protected $tag;

	/**
	 * Set the HTML tag
	 *
	 * @param  string  $tag
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function tag($tag) {
		if(!$this->canBeString($tag)) throw new Exception('Tag has to be scalar value');

		$this->tag = (string) $tag;
		return $this;
	}

	/**
	 * Set tag attributes
	 *
	 * @param  string  $key
	 * @param  string  $value
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function attr($key, $value) {
		if(!$this->canBeString($key)) throw new Exception('Key has to be scalar value');
		if(!$this->canBeString($value)) throw new Exception('Value has to be scalar value');

		$this->attributes[(string) $key] = (string) $value;
		return $this;
	}

	/**
	 * Set jQuery data attributes
	 *
	 * @param  string  $key
	 * @param  string  $value
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function data($key, $value) {
		if(!$this->canBeString($key)) throw new Exception('Key has to be scalar value');
		if(!$this->canBeString($value)) throw new Exception('Value has to be scalar value');

		$this->attributes['data-' . (string) $key] = (string) $value;
		return $this;
	}

	/**
	 * Set content of tag as text
	 *
	 * @param  string  $content
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function text($content) {
		if(!$this->canBeString($content)) throw new Exception('Content has to be scalar value');

		$this->children[] = htmlentities((string) $content);
		return $this;
	}

	/**
	 * Set content of tag as html
	 *
	 * @param  string  $content
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function html($content) {
		if(!$this->canBeString($content)) throw new Exception('Content has to be scalar value');

		$this->children[] = (string) $content;
		return $this;
	}

	/**
	 * Append other instance
	 *
	 * @param  \Comayers\DOM\DOMBuilder  $child
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function append($child) {
		if(!$child instanceof self) throw new Exception('Child must be instance of DOMBuilder');

		$this->children[] = $child;
		return $this;
	}

	/**
	 * Append a child tag
	 *
	 * @param  string    $tag
	 * @param  \Closure  $callback
	 * @return \Comayers\DOM\DOMBuilder
	 */
	public function __call($tag, $callback) {
		$dom = new DOM;
		$this->children[] = call_user_func_array(array($dom,$tag), $callback);
		return $this;
	}

	/**
	 * Create HTML DOM
	 *
	 * @param  \DOMDocument  $doc
	 * @return \DOMElement
	 */
	public function createDOM($dom) {
		$element = $dom->createElement($this->tag);

		foreach($this->attributes as $key => $value) {
			$element->setAttribute($key, $value);
		}

		foreach($this->children as $childContents) {
			if($childContents instanceof self) {
				$child = $childContents->createDOM($dom);
			} else {
				$child = $dom->createCDATASection($childContents);
			}
			$element->appendChild($child);
		}

		return $element;
	}

	/**
	 * Converts the tree to HTML code
	 *
	 * @return string
	 */
	public function __toString() {
		try {
			$dom = new DOMDocument();
			$new = $this->createDOM($dom);
			$dom->appendChild($new);
			return str_replace(array('<![CDATA[',']]>'),'',$dom->saveHTML());
		} catch(Exception $e) {
			return 'exception: ' . $e->getMessage();
		}
	}

	private function canBeString($value) {
		return ((is_object($value) && method_exists($value, '__toString')) || is_null($value) || is_scalar($value));
	}
}