<?php namespace Comayers\DOM\Facades;

use Illuminate\Support\Facades\Facade;

class DOM extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'dom'; }

}